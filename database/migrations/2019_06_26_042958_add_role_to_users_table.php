<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoleToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
			$table->enum('role', ['admin','user']);
			$table->string('phone_number')->after('role')->nullable();
			$table->string('address')->after('phone_number')->nullable();
			$table->string('city')->after('address')->nullable();
			$table->string('state')->after('city')->nullable();
			$table->string('post_code')->after('state')->nullable();
			$table->string('profile_picture')->after('post_code')->nullable();
			$table->string('description')->after('profile_picture')->nullable();
			$table->enum('status', ['0','1']);
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
