<header>
   <div class="top_hdr">
      <div class="container">
        <div class="div_contant">
          <ul>
              <li><a href="#"><i class="fa fa-phone" aria-hidden="true"></i>0987654321</a></li> 
              <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i>abg@gmail.com</a></li> 
           </ul> 
         </div>
          
           <div class="div_contant" style="text-align: center; margin: auto">
                <div class="right_nav">
      
          <ul>
           
             
              
            
                  <li><input type="text" class="srch" placeholder="Search"></li> 
                <li><a href="/wishlistproducts"><i class="fa fa-heart" aria-hidden="true"></i></a></li> 
         <li><a href="http://craftfort.localhost/cartproducts"><i class="fa fa-shopping-cart" aria-hidden="true"></i>
         <?php $cartCollection = Cart::getContent(); 
             $count = $cartCollection->count();
              ?>
             <span class="caert_div"><?php echo $count;?></span>
                 
             
              </li>
           </ul> 
  
      
      </div>
         </div>
        <div class="div_contant" style="text-align: right">
                 <div class="dropdown">
          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">English
          <span class="caret"></span></button>
          <ul class="dropdown-menu">
            <li><a href="#">Spanish</a></li>

          </ul>
        </div>

                    <div class="dropdown">
          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">$USD
          <span class="caret"></span></button>
          <ul class="dropdown-menu">
            <li><a href="#">RUPEE</a></li>
            <li><a href="#">$USD</a></li>

          </ul>
		  @guest
                           
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
								
								<a href="/login">My Account</a>
								<a href="/changepassword">Change Password</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
							 
							
                        @endguest
        </div>
         </div>
          
       
          
          
          
       </div>
    </div>    
    
    
    
    
    <!----navigation---->
    <nav class="navbar navbar-inverse">
  <div class="container">

    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
      </button>
      <a class="navbar-brand" href="/"><img src="{{ asset('public/images/logo1.png') }}"></a>
    </div>
    <div class="collapse navbar-collapse navbar-right" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="{{ (request()->is('/')) ? 'active' : '' }}" ><a href="/">Home</a></li>
        <li class="{{ (request()->is('about')) ? 'active' : '' }}"> <a href="/about">About us</a></li>
        <li class="{{ (request()->is('products')) ? 'active' : '' }}"><a href="/get_products">Products</a></li> 
        <li class="{{ (request()->is('contact')) ? 'active' : '' }}"><a href="/contact">Contact Us</a></li> 
      </ul>
     
    </div>
 
  </div>
</nav>

    <!----navigation---->
    
</header>    
    
<!--header--->