<footer>
      <div class="container">
         <div class="row">
            <div class="col-sm-3">
               <div class="ftr_inner">
                  <h4>CATEGORIES</h4>
                     <ul>
                       <li><a href="#">Lorem ipsum dolo</a></li>
                       <li><a href="#">Lorem ipsum dolo</a></li>
                       <li><a href="#">Lorem ipsum dolo</a></li>
                       <li><a href="#">Lorem ipsum dolo</a></li>
                   </ul>
                </div>
             </div>
             
                <div class="col-sm-3">
               <div class="ftr_inner">
                  <h4>CUSTOMER SERVICE</h4>
                     <ul>
                       <li><a href="#">TERMS OF USE</a></li>
                       <li><a href="#">PRIVACY POLICY</a></li>
                       <li><a href="#">F.A.Q.</a></li>
                       <li><a href="#">CONTACT INFO</a></li>
                   </ul>
                </div>
             </div>
             
              <div class="col-sm-3">
               <div class="ftr_inner">
                  <h4>INFORMATION</h4>
                     <ul>
                       <li><a href="#">ABOUT US</a></li>
                       <li><a href="#">OUR PORTFOLIO</a></li>
                       <li><a href="#">OUR PORTFOLIO</a></li>
                       <li><a href="#">ARRIVAL SALES</a></li>
                   </ul>
                </div>
             </div>   
             
             
             <div class="col-sm-3">
               <div class="ftr_inner">
                  <h4>INFORMATION</h4>
                  <ul>
                                <li><span><span class="h6-style">Call Us:</span><span>1234567890</span></span></li>
                                <li><span><span class="h6-style">Hours:</span><span>Mon-fri 9am-8pm<br>sat 9am-6pm</span></span></li>
                                <li><span><span class="h6-style">E-mail:</span><span><a href="mailto:info@goodwin.us">info@avb.us</a></span></span></li>
                                <li><span><span class="h6-style">Address:</span><span>abc</span></span></li>
                            </ul>
                </div>
             </div>
             
             
          </div>  
      </div>
    
        <div class="container">
         <div class="copy_right">
          <p>© COPYRIGHT 2019 abc. ALL RIGHTS RESERVED.</p>
             
             <ul>
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
             </ul>
             </div>
        </div>
    </footer>
    <!----footer---->