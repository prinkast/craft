@extends('layouts.main')
@section('content')

<!----CART-DIV----->
    <div class="cart_div">
        <div class="container">
            <div class="cart_inner">
              <h2>Wishlist</h2>
             
            	<form action="#" method="post" enctype="multipart/form-data">
                  	<div class="table-responsive cart-info">
	                    <table class="table table-bordered">
	                     	<thead>
		                        <tr>
			                        <td class="text-center">Image</td>
			                        <td class="text-center hidden-xs">Product Name</td>
			                        <td class="text-center">price</td>
			                         <td class="text-center">Action</td>
		                        </tr>
	                      	</thead>
	                      	<tbody>
		                      	

									@foreach ($wishlists as $wishlist)
									
									
										
										<?php 
										$img = json_decode($wishlist->image);
										
										
 ?>
										<tr>
				                          	<td class="text-center">            
				                              	<a href="#"><img src="{{ URL::asset('/public/products/'.$img[0])}}" class="cart_img"></a>
				                           	</td>
				                          	<td class="text-center hidden-xs">
				                              	<a href="#">{{ $wishlist->product_name }}</a>
				                            </td>
				                          	
				                          	<td class="text-center hidden-xs"> {{ $wishlist->sell_price }}</td>
				                          	
				                          	<td class="text-center btn_cart">
				                          		
				                              	<a href="{{ url('delete/'.$wishlist->id)}}">Remove</a>
				                          	</td>
		                    			</tr>
		                    			
										
                              		@endforeach       
                            </tbody>
                    	</table>
                  	</div>
                </form>
            </div>
        </div> 
    </div>
<!----CART-DIV----->













@endsection