<!-- begin::Footer -->
		<footer class="m-grid__item		m-footer ">
			<div class="m-container m-container--fluid m-container--full-height m-page__container">
				<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
					<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">	<span class="m-footer__copyright">
					2017 &copy; Craftfort theme by <a href="https://keenthemes.com/" class="m-link">Keenthemes</a>
				</span>
					</div>
					<div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
						<ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
							<li class="m-nav__item">
								<a href="#" class="m-nav__link">	<span class="m-nav__link-text">About</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="#" class="m-nav__link">	<span class="m-nav__link-text">Products</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="#" class="m-nav__link">	<span class="m-nav__link-text">Contact</span>
								</a>
							</li>
							<li class="m-nav__item">
								<a href="#" class="m-nav__link">	<span class="m-nav__link-text">Login</span>
								</a>
							</li>
							<li class="m-nav__item m-nav__item">
								<a href="#" class="m-nav__link" data-toggle="m-tooltip" title="Support Center" data-placement="left">	<i class="m-nav__link-icon flaticon-info m--icon-font-size-lg3"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</footer>
		<!-- end::Footer -->
	</div>
	<!-- end:: Page -->
	<!-- begin::Quick Sidebar -->
	
	<!-- end::Quick Sidebar -->
	<!-- begin::Scroll Top -->
	<div id="m_scroll_top" class="m-scroll-top">	<i class="la la-arrow-up"></i>
	</div>
	<!-- end::Scroll Top -->
	<!-- begin::Quick Nav -->
	<ul class="m-nav-sticky" style="margin-top: 30px;">
		<li class="m-nav-sticky__item" data-toggle="m-tooltip" title="facebook" data-placement="left">	<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
		</li>
		<li class="m-nav-sticky__item" data-toggle="m-tooltip" title="twitter" data-placement="left">	<a href="#" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
		</li>
		<li class="m-nav-sticky__item" data-toggle="m-tooltip" title="youtube" data-placement="left">	<a href="#" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a>
		</li>
		<li class="m-nav-sticky__item" data-toggle="m-tooltip" title="instagram" data-placement="left">	<a href="#" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
		</li>
	</ul>
	