@extends('backend.layouts.main')

@section('content')
		<!-- END: Header -->
	
			<!-- END: Left Aside -->
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="page-header">
							<div class="container-fluid">
								<div class="pull-right">
									<!--button type="button" data-toggle="tooltip" class="btn btn-default hidden-md hidden-lg"><i class="fa fa-filter"></i>
									</button>
									<a type="button" class="btn btn-primary"  href="javascript:void(0)" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i>
									</a>
									<!--button type="submit" form="form-product" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Copy"><i class="fa fa-copy"></i>
									</button>
									<button type="button" form="form-product" data-toggle="modal" data-target="#myModal1" title="" class="btn btn-danger"><i class="fa fa-trash-o"></i>
									</button-->
                               
								</div>
								<h1>Orders</h1>
								
							</div>
						
						<div class="container-fluid">
							<div class="row">
							<div class="col-md-12 col-sm-12">
									<div class="panel panel-default">
										
										<div class="panel-body">
											<form id="form-product">
												<div class="table-responsive">
												
													<table class="table table-bordered table-hover" id="table_order">
														<thead>
															<tr>
																<td style="width: 1px;" class="text-center" >
																	id
																</td>
																
																<td class="text-left"> <a href="#" class="asc">product_id</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc">user id</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc">Address</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc"> state</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc"> city</a> 
																</td>
																<td class="text-left"> <a href="#" class="asc"> Pin Code</a> 
																</td>
																<td class="text-left"> <a href="#">product price</a> 
																</td>
																<td class="text-left"> <a href="#">Total Price</a> 
																</td>
																<td class="text-left"> <a href="#">discount_coupon </a> 
																</td>
																<td class="text-left"> <a href="#">Status</a> 
																</td>
																<!--td class="text-left"> <a href="#">Image</a> 
																</td>
																<td class="text-left">Action</td-->
															</tr>
														</thead>
														@foreach($order as $orders)
														<tbody>
														<?php
														//echo"<pre>";
														//var_dump($order);die;
														?>
														
                                                          <td>{{$orders->id}}</td>
                                                          <td>{{$orders->product_id}}</td>
                                                          <td>{{$orders->user_id}}</td>
                                                          <td>{{$orders->address}}</td>
                                                          <td>{{$orders->state}}</td>
                                                          <td>{{$orders->city}}</td>
                                                          <td>{{$orders->zipcode}}</td>
                                                          <td>{{$orders->product_price}}</td>
                                                          <td>{{$orders->product_total}}</td>
                                                          <td>{{$orders->discount_coupon}}</td>
                                                          <td>{{$orders->status}}</td>
                                                          
															
														
														
														</tbody>
														@endforeach	
													</table>
												</div>
											</form>
											<div class="row">
												<div class="col-sm-6 text-left">
													<ul class="pagination">
														<li class="active"><span>1</span>
														</li>
														<li><a href="#">2</a>
														</li>
														<li><a href="#">&gt;</a>
														</li>
														<li><a href="#">&gt;|</a>
														</li>
													</ul>
												</div>
												<div class="col-sm-6 text-right">Showing 1 to 20 of 21 (2 Pages)</div>
											</div>
										</div>
									</div>
								</div>
								<!--div id="filter-product" class="col-md-3 col-md-push-9 col-sm-12 hidden-sm hidden-xs">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title"><i class="fa fa-filter"></i> Filter</h3>
										</div>
										<div class="panel-body">
											<div class="form-group">
												<label class="control-label" for="input-name">Product Name</label>
												<input type="text" name="filter_name" value="" placeholder="Product Name" id="input-name" class="form-control" autocomplete="off">
												<ul class="dropdown-menu"></ul>
											</div>
											<div class="form-group">
												<label class="control-label" for="input-model">Model</label>
												<input type="text" name="filter_model" value="" placeholder="Model" id="input-model" class="form-control" autocomplete="off">
												<ul class="dropdown-menu"></ul>
											</div>
											<div class="form-group">
												<label class="control-label" for="input-price">Price</label>
												<input type="text" name="filter_price" value="" placeholder="Price" id="input-price" class="form-control">
											</div>
											<div class="form-group">
												<label class="control-label" for="input-quantity">Quantity</label>
												<input type="text" name="filter_quantity" value="" placeholder="Quantity" id="input-quantity" class="form-control">
											</div>
											<div class="form-group">
												<label class="control-label" for="input-status">Status</label>
												<select name="filter_status" id="input-status" class="form-control">
													<option value=""></option>
													<option value="1">Enabled</option>
													<option value="0">Disabled</option>
												</select>
											</div>
											<div class="form-group text-right">
												<button type="button" id="button-filter" class="btn btn-default"><i class="fa fa-filter"></i> Filter</button>
											</div>
										</div>
									</div>
								</div-->
								
							</div>
						</div>
					</div>
					<!-- END: Subheader -->
				</div>
			</div>
			<!--------end -UNDER-dashboard------------------->
		</div>

	</div>
	
	<!---SCRIPT---->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<script src="public/js/scripts.bundle.js"></script>
	<script src="public/js/vendors.bundle.js"></script>
    
    
	
    
    
    
	<!-----popup------>
	<!-- Modal -->
	
    
<!----scd---pupup----->
    
    <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        </div>
 
          <div class="delet">
             <h6>www.Craftfort.com says</h6>
              <p>Are you sure?</p>
            
            </div>
     
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancle</button>
        </div>
      </div>
      
    </div>
  </div>
  
	 <script type="text/javascript">
  $(function() {
	  
	   $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });
               $('#table_order').DataTable({
               processing: true,
               serverSide: true,
               ajax: '{{ url('order') }}',
               columns: [
                        { data: 'product_id', name: 'product_id' },
                        { data: 'user_id', name: 'user_id' },
                        { data: 'address', name: 'address' },
                        { data: 'city', name: 'city' },
                        { data: 'state', name: 'state' },
                        { data: 'product_price', name: 'product_price' },
                        
                        { data: 'product_total', name: 'product_total'},
                        { data: 'status', name: 'status' },
                      /*  {
				data: 'image',
				"render": function(data, type, row) {
					return '<img src="http://craftfort.localhost/public/products/'+data+'" style="height:100px;width:100px;" />';
				} 
			}, */
			 {data: 'action', name: 'action', orderable: false, searchable: false},
                      
                     ]
            });
       
		 
		  $("form#data").submit(function(e) {
    e.preventDefault();    
    var formData = new FormData(this);

    $.ajax({
       url: '{{ url('order') }}',
        type: 'POST',
        data: formData,
        success: function (data) {
           
        },
        cache: false,
        contentType: false,
        processData: false,
		 success: function (data) {
     
              $('#productForm').trigger("reset");
              $('#ajaxModel').modal('hide');
			   window.location.reload();
              table.draw();
         
          },
          error: function (data) {
              console.log('Error:', data);
              $('#saveBtn').html('Save Changes');
          }
    });
		  });
	
	 $('body').on('click', '.deleteProduct', function () {
     
        var product_id = $(this).data("id");
		//alert(product_id);
        //confirm("Are You sure want to delete !");
      
        $.ajax({
            type: "GET",
			
           url: '{{ url('admin/product_delete') }}'+'/'+product_id,
            success: function (data) {
				 window.location.reload();
                table.draw();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });
	
	  $('body').on('click', '.editProduct', function () {
      var product_id = $(this).data('id');
	  
      $.get('{{ url('admin/product_edit') }}' +'/' + product_id , function (data) {
	
          $('#modelHeading').html("Edit Product");
          $('#saveBtn').val("edit-user");
          $('#product_id').val(data.id);
          $('#cat_id').val(data.cat_id);
          $('#product_name').val(data.product_name);
          $('#product_desc').val(data.product_desc);
          $('#price').val(data.price);
          $('#sell_price').val(data.sell_price);
          $('#quantity').val(data.quantity);
          $('#total_price').val(data.total_price);
          $('#status').val(data.status);
          $("#ajaxModel").modal('show');
		  
         
        
      })
   });

  });
</script>
				@endsection	 