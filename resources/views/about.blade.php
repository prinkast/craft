@extends('layouts.main')
@section('content')
<!---about_Banner---->
    <div class="abt_banner">
      <div class="container">
          <div class="abt_bnner-iner">
            <h1>About Us</h1>
              <ul>
               <li><a href="#">Home</a></li>
               <li>/ About Us</li>
              </ul>
          
          </div>
        
        
        </div>
    
    </div>
<!---about_banner---->

<!----abt_sec2----->
    <div class="abt_sec2">
       <div class="container">
           <div class="row">
              <div class="col-sm-6">
                 <div class="abt_sec2_txt">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                     
                     <a href="#">Read More</a>
                  </div>
               </div>
           
               <div class="col-sm-6">
                  <div class="abt_sec2_img">
                     <img src="{{ asset('public/images/img2.png') }}">
                   
                   </div>
               
               </div>
           
           </div>
        
        </div>
    
    </div>
<!----abt_sec2----->

    
<!----why_sec---->
    <div class="why_sec">
       <div class="container">
           <h2>Why Us</h2>
<div class="row vert-margin">
                    <div class="col-sm-3">
                        <div class="block-it text-center">
                            <div class="block-it-icon"><i class="fa fa-archive" aria-hidden="true"></i></div>
                            <h4 class="text-uppercase">Free worlwide delivery</h4>
                            <p>Lorem ipsum dolor sit amet conset ctetur adipisicing elit, sed do eiusmod tempor incidid.</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="block-it text-center">
                            <div class="block-it-icon"><i class="fa fa-tag" aria-hidden="true"></i></div>
                            <h4 class="text-uppercase">Promotions, bonuses and discounts</h4>
                            <p>Lorem ipsum dolor sit amet conset ctetur adipisicing elit, sed do eiusmod tempor incidid.</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="block-it text-center">
                            <div class="block-it-icon"><i class="fa fa-usd" aria-hidden="true"></i></div>
                            <h4 class="text-uppercase">Free secret Reward Card</h4>
                            <p>Lorem ipsum dolor sit amet conset ctetur adipisicing elit, sed do eiusmod tempor incidid.</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="block-it text-center">
                            <div class="block-it-icon"><i class="fa fa-gift" aria-hidden="true"></i></div>
                            <h4 class="text-uppercase">Presents to our customers</h4>
                            <p>Lorem ipsum dolor sit amet conset ctetur adipisicing elit, sed do eiusmod tempor incidid.</p>
                        </div>
                    </div>
                </div>
        
        
        </div>
    
    
    </div>
<!----why_sec---->  
    
    
<!----who_sec---->
    <div class="who_sec">
       <div class="container">
          <h2>WHO WE ARE</h2> 
           <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat.</p>
           
           <img src="{{ asset('public/images/abt_img.jpg') }}">
             
              <div class="cont_in">
                  <span>Quality</span>
                  <div class="counter" data-count="150">0</div>
               </div>
              <div class="cont_in">
                   <span>Regular Buyers</span>
                    <div  class="counter"data-count="85">0</div>
              </div>
                 <div class="cont_in">
                     <span>Product</span>
                    <div class="counter" data-count="2200">0</div>
                  </div>
           
        </div>    
        
        

        
        
    </div>
<!----who_sec---->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> 
<script>
    $('.counter').each(function() {
  var $this = $(this),
      countTo = $this.attr('data-count');
  
  $({ countNum: $this.text()}).animate({
    countNum: countTo
  },

  {

    duration: 8000,
    easing:'linear',
    step: function() {
      $this.text(Math.floor(this.countNum));
    },
    complete: function() {
      $this.text(this.countNum);
      //alert('finished');
    }

  });  
  
  

});
    </script>   
    
@endsection