@extends('layouts.main')
@section('content')

<!----CART-DIV----->
    <div class="cart_div">
        <div class="container">
            <div class="cart_inner">
              <h2>Shopping Cart</h2>
             
            	<form action="#" method="post" enctype="multipart/form-data">
                  	<div class="table-responsive cart-info">
	                    <table class="table table-bordered">
	                     	<thead>
		                        <tr>
			                        <td class="text-center">Image</td>
			                        <td class="text-center hidden-xs">Product Name</td>
			                        <td class="text-center">Quantity</td>
			                        <td class="text-center hidden-xs">Unit Price</td>
			                        <td class="text-center">Total</td>
			                        <td class="text-center">Action</td>
		                        </tr>
	                      	</thead>
	                      	<tbody>
		                      	@if($total_items == 0)
									<p style="color:black;"> Cart is Empty</p>
				
								@else

									@foreach ($items as $item)
										@foreach ($item->attributes as $attribute) 
										<?php 
										//var_dump($item->id);
										$img = json_decode($attribute['image']);
 ?>
										<tr>
										
				                          	<td class="text-center">            
				                              	<a href="#"><img src="{{ URL::asset('/public/products/'.$img[0])}}" class="cart_img"></a>
				                           	</td>
				                          	<td class="text-center hidden-xs">
				                              	<a href="#">{{ $item->name }}</a>
				                            </td>
				                          	<td class="text-center hidden-xs">
			                          			<input type="number" name="quantity" value="{{ $item->quantity }}"  class="num_in" id="{{ $item->id.'quan'}}">
                         						
				                          	 	
				                          	</td>
				                          	<td class="text-center hidden-xs"> {{ $item->price }}</td>
				                          	<td class="text-center">{{ $item->price * $item->quantity }}</td>
				                          	<td class="text-center btn_cart">
				                          		<a href="javascript:void(0)" onclick="UpdateItemQuantity('{{ $item->id }}','{{ $item->id.'quan'}}')">update</a>
				                              	<a href="javascript:void(0)" onclick="RemoveItem('{{ $item->id }}')">Remove</a>
				                          	</td>
		                    			</tr>
										
		                    			@endforeach
                              		@endforeach       
                            </tbody>
                              	<p style="color:black; display:inline-block;">Total : {{ $total }}</p>
								<a href="/checkout" class="chck_place">Place Order</a>
                              	@endif
								
						 
                    	</table>
						
                  	</div>
                </form>
				
				<!--form action="/checkout"  method="post">
						  {{csrf_field()}}
						  @foreach ($items as $item)
						  <input name="user_id" type="hidden" value="{{Auth::user()->id}}" />
						  <input name="product_id" type="hidden" value="{{$item->id}}" />
						 @endforeach 
						  <input name="submit" type="submit" value="Place Order"  /> 
						      
						</form-->
            </div>
        </div> 
    </div>
<!----CART-DIV----->













@endsection