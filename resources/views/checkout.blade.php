@extends('layouts.main')
@section('content')
<div class="account_div2" >
        <div class="overlay"></div>
       <div class="container">
          <div class="act-inner" >

    <?php
	$users = json_decode(json_encode($user), true);
	?>
	
               
				<h2>checkout</h2>
                <div class="card-body">
                    <form method="POST" action="/checkout">
                        @csrf
                 
                       <div class="from-group from-group2">
                            <label for="name" >{{ __('Name') }}</label>

                          
                                <input id="name" type="text" class="frm_in form-control @error('name') is-invalid @enderror" name="name" value="{{ $users['name'] }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div>
						

                       <div class="from-group from-group2">
                            <label for="email" >{{ __('E-Mail Address') }}</label>

                            
                                <input id="email" type="email" class="frm_in form-control @error('email') is-invalid @enderror" name="email" value="{{ $users['email'] }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div>
						 <div class="from-group from-group2">
                            <label for="phone_number" >{{ __('Phone') }}</label>

                            
                                <input id="phone_number" type="number" class="frm_in form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ $users['phone_number'] }}" required autocomplete="phone_number">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div>
                          
						   <div class="from-group from-group2">
                            <label for="state" >{{ __('state') }}</label>

                          
                                <input id="state" type="text" class="frm_in form-control @error('state') is-invalid @enderror" name="state" required autocomplete="new-state">

                                @error('state')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div>
						
						 <div class="from-group from-group2">
                            <label for="city" >{{ __('city') }}</label>

                          
                                <input id="city" type="text" class="frm_in form-control @error('city') is-invalid @enderror" name="city" required autocomplete="new-city">

                                @error('city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div>
						
                        <div class="from-group from-group2">
                            <label for="address" >{{ __('Address') }}</label>

                          
                                <input id="address" type="text" class="frm_in form-control @error('address') is-invalid @enderror" name="address" required autocomplete="new-address">

                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div>
						
						
						
						
						
						 <div class="from-group from-group2">
                            <label for="post_code" >{{ __('Pin code') }}</label>

                          
                                <input id="post_code" type="text" class="frm_in form-control @error('post_code') is-invalid @enderror" name="post_code" required autocomplete="new-post_code">

                                @error('post_code')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div>
						
						 <!--div class="from-group from-group2">
                            <label for="description" >{{ __('Description') }}</label>

                          
                                <textarea id="description" type="text" class="frm_in form-control @error('description') is-invalid @enderror" name="description" required autocomplete="new-description"></textarea>

                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                        </div-->

                       
                     
                       <div class="">
				<!--p>I Agree to<a href="#"> Terms and Conditions</a></p-->
                                 <button class="sub_button">
                                    {{ __('Checkout') }}
                                </button>
                     
                        </div>
                    </form>
					
                </div>
				
            </div>
        </div>
    </div>

@endsection

<style>
nav.navbar.navbar-expand-md.navbar-light.bg-white.shadow-sm {
    display: none;
}
</style>