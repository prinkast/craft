<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
	protected $table = 'order';
    protected $fillable = [
       'product_id', 'user_id', 'address','state','city','zipcode','product_price','product_total','discount_coupon','status'];
}
