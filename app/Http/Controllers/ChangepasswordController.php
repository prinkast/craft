<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Profile;
use Auth;
use DB;
use File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Validator;
 use Hash;
 use App\User;
class ChangepasswordController extends Controller {
public function changepasswords(){
		
			return view('changepassword');
		}
   
   
   public function changepassword(Request $request){
		if(Auth::Check()){	
			 $validator = Validator::make($request->all(), [
			
			'oldpassword' => 'required',
			 'password' => 'required',
			 'confirmpassword' =>'required|same:password'
			
			]);
			if($validator->fails()){
	$messages = $validator->messages();			 return Redirect::back()->withErrors($validator);
			 } 
			 else{
				 
				 $oldpassword=$request->oldpassword;
				 $newpassword=$request->password;		
				 $confirmpassword=$request->confirmpassword;				 				 				
				 $currentpasscheck=Auth::user()->password;				 			
				 if (Hash::check($oldpassword, $currentpasscheck)) {					  		
				 }else{					  					             
				 return redirect()->back()->with('alert-danger','Old  password is incorrect');
				 exit;				  }				 				
				 if($confirmpassword != $newpassword){				
				 return redirect()->back()->with('alert-danger','Your new password  is not match with confirm password');					
				 exit;				
				 }
				
				
			 if (Hash::check($oldpassword, $currentpasscheck)) {				 
						 $user_id = Auth::user()->id;
						$obj_user = user::find($user_id);
						$obj_user->password =bcrypt($newpassword);
						$obj_user->save(); 
						return redirect()->back()->with('alert-success','change password successfully');
        }
      
		
		
		
					
			
			}
		}
		
			
	}
}