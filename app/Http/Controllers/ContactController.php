<?php

namespace App\Http\Controllers;
use App\Contact;
use Illuminate\Http\Request;
use Validator;
use MetaTag;

use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;
class ContactController extends Controller
{
    public function index()
    {

        return view('contact');
    }
	public function create(Request $request){
		
		
		$validator = Validator::make($request->all(), [
		
		'firstname' => 'required',
		'lastname' => 'required',
		'email' => 'required',
		'address' => 'required',
		'number' => 'required',
		'pincode' => 'required',
		'comment' => 'required',
		
        
		]);
		if($validator->fails()){
			 return redirect()->back()->with('success','please fill all fields');
         } 
		else{
		$user=new Contact;
		$user->firstname=$request->firstname;
		$user->lastname=$request->lastname;
		$user->email=$request->email;
		$user->address=$request->address;
		$user->number=$request->number;
		$user->pincode=$request->pincode;
		$user->comment=$request->comment;
		// echo"<pre>";
		// print_r($user);die;
		$subject="contact us mail";
		$from=$request->email;
		//Mail::to("aanchalkhera.st@gmail.com")->send(new SendMail($subject,$request->comment));
        $user->save();
		return redirect()->back()->with('success','Message has been sent successfully');
		
	}
}
public function about()
    {
        return view('about');
    }
}
