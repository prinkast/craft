<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use App\User;
use Illuminate\Support\Facades\Facade;
use Yajra\DataTables\Services\DataTable;
use file;
class CustomerController extends Controller
{
     public function index(Request $request)
    {
   
       
      if ($request->ajax()) {
            $data = User::latest()->where('role','user')->get();
			
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
   
                           $btn = '<a href="javascript:void(0)"     data-id="'.$row->id.'" data-original-title="Edit" form="form-customer" data-toggle="modal" data-target="#myModal"  class="edit btn btn-primary btn-sm editCustomer">Edit</a>';
   $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteCustomer">Delete</a>';
    
                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        return view('backend/customer');
    
      
       
    }
     
   public function store(Request $request)
    {
		 $user=User::select('password')->where('id',$request->customer_id)->first();
		 $users=$user->password;
		if(isset($request->customer_id)){
        $data1=User::select('profile_picture')->where('id',$request->customer_id)->first();
		
		$mnt = $data1->profile_picture;}
		
		if($request->profile_picture!=''){
		   $uploaddir='public/customer';
		   $images = basename($_FILES['profile_picture']['name']);
		   $temp_name=$_FILES['profile_picture']['tmp_name'];
           $directory=$uploaddir.'/'.$images;
          // var_dump($directory);die;
		 move_uploaded_file($temp_name,$directory );
		
		} 
		else{
			$images= $mnt;
		}

        User::where('id', $request->customer_id)->
                update(['name'=>$request->customer_name,'email' => $request->email,'password'=>$users, 'phone_number' => $request->phone,'address' => $request->address, 'city' => $request->city,'state' => $request->state, 'post_code' => $request->postcode, 'description' => $request->description,'status' => $request->status,'profile_picture' => $images]);        
   
        return response()->json(['success'=>'Customer added successfully.']); 
    }
	
	 public function edit($id)
    {
        $customer = User::find($id);
		//var_dump($customer);die;
        return response()->json($customer);
    }
  
   
   
    public function destroy($id)
    {
		//var_dump($id);die;
        User::find($id)->delete();
     
        return response()->json(['success'=>'customer deleted successfully.']);
    }
}
